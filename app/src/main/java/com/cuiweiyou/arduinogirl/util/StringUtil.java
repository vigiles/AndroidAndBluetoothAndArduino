package com.cuiweiyou.arduinogirl.util;

public class StringUtil {

    /**
     * 将蓝牙模块收到的若干ASCII码转字符串
     *
     * @param bytes ASCII码数据
     * @return
     */
    public static String getChars(byte[] bytes) {
        if (null != bytes && bytes.length > 0) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                char chacter = (char) bytes[i];
                sb.append(chacter);
            }

            return sb.toString();
        }

        return "";
    }
}
