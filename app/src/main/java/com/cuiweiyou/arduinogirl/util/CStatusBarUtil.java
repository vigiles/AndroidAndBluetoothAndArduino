package com.cuiweiyou.arduinogirl.util;

import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

public class CStatusBarUtil {

    /**
     * 设置Activity对应的顶部状态栏的颜色
     * @param activity 目标Activity
     * @param colorResId 在res/values/colors.xml中定义的颜色
     */
    public static void setWindowStatusBarColor(Activity activity, int colorResId) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = activity.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(activity.getResources().getColor(colorResId, null));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
