package com.cuiweiyou.arduinogirl.base;

import android.app.Application;
import android.content.Context;

/**
 * 应用上下文
 */
public class BluetoothApp extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

    }

    public static Context getContext() {
        return context;
    }
}
