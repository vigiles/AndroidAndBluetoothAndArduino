package com.cuiweiyou.arduinogirl.ui;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanResult;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuiweiyou.catwristhands.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macpro on 2018/10/24.
 */

public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.BluetoothHolder> {

    private List<ScanResult> bluetoothList;
    private OnDeviceClickListener onDeviceClickListener;
    private ScanResult selectDevice;
    private final int colorff0000;
    private final int color000000;

    public DevicesAdapter(List<ScanResult> list) {
        this.bluetoothList = list;
        colorff0000 = Color.parseColor("#ff0000");
        color000000 = Color.parseColor("#000000");
    }

    @Override
    public BluetoothHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_devices, null);
        return new BluetoothHolder(view);
    }

    @Override
    public void onBindViewHolder(BluetoothHolder holder, int position) {
        ScanResult model = bluetoothList.get(position);
        BluetoothDevice device = model.getDevice();
        int rssi = model.getRssi();
        String name = device.getName();
        String address = device.getAddress();

        holder.nameTextView.setText(name + "");
        holder.macTextView.setText(address + "");
        holder.rssiTextView.setText(rssi + "");

        if (null != selectDevice) {
            String addr = selectDevice.getDevice().getAddress();
            if (addr.equals(address)) {
                holder.nameTextView.setTextColor(colorff0000);
                holder.macTextView.setTextColor(colorff0000);
                holder.rssiTextView.setTextColor(colorff0000);
                holder.imageView.setImageResource(R.mipmap.connected);
            } else {
                holder.nameTextView.setTextColor(color000000);
                holder.macTextView.setTextColor(color000000);
                holder.rssiTextView.setTextColor(color000000);
                holder.imageView.setImageResource(R.mipmap.connected_diss);
            }
        } else {
            holder.nameTextView.setTextColor(color000000);
            holder.macTextView.setTextColor(color000000);
            holder.rssiTextView.setTextColor(color000000);
            holder.imageView.setImageResource(R.mipmap.connected_diss);
        }
    }

    @Override
    public int getItemCount() {
        return null == bluetoothList ? 0 : bluetoothList.size();
    }

    public void updateDevices(List<ScanResult> list) {
        if (null != bluetoothList) {
            this.bluetoothList.clear();
        }

        this.bluetoothList = new ArrayList<>(list);
        notifyDataSetChanged();
    }

    public void setSelected(ScanResult o) {
        selectDevice = o;
        notifyDataSetChanged();
    }

    class BluetoothHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private View deviceLayout;
        private TextView nameTextView;
        private TextView macTextView;
        private TextView rssiTextView;
        private ImageView imageView;

        public BluetoothHolder(View view) {
            super(view);

            deviceLayout = view.findViewById(R.id.deviceLayout);
            nameTextView = view.findViewById(R.id.nameTextView);
            macTextView = view.findViewById(R.id.macTextView);
            rssiTextView = view.findViewById(R.id.rssiTextView);
            imageView = view.findViewById(R.id.imageView);

            deviceLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            ScanResult model = bluetoothList.get(position);
            if (null != onDeviceClickListener) {
                onDeviceClickListener.onDeviceClick(model);
            }
        }
    }

    public void setOnDeviceClickListener(OnDeviceClickListener listener) {
        this.onDeviceClickListener = listener;
    }

    public interface OnDeviceClickListener {
        void onDeviceClick(ScanResult model);
    }
}
