package com.cuiweiyou.arduinogirl.model;

import java.io.Serializable;

/**
 * 和蓝牙的连接状态
 * Created by macpro on 2018/11/5.
 */
public class EventGattedModel implements Serializable {
    private int flag;

    /**
     * 和蓝牙的连接状态
     *
     * @param i 1连接了，0断开了
     */
    public EventGattedModel(int i) {
        this.flag = i;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "EventGattedModel{" + "flag=" + flag + '}';
    }
}
