package com.cuiweiyou.arduinogirl.model;

import java.io.Serializable;

/**
 * Arduino发来的消息
 */
public class EventArduinoModel implements Serializable {

    String msg;

    public EventArduinoModel(String msg) {
        this.msg = msg;
    }

    /**
     * Arduino发来的消息
     */
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "EventArduinoModel{" + "msg='" + msg + '\'' + '}';
    }
}
